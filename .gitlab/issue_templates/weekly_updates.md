<!-- Ensure the title is: Weekly Updates MONTH DAY, YEAR (YYYY-MM-DD) -->

_Please remove yourself from the assignees list after you have read the announcements._


## :book: Please Review

- [Engineering Week In Review](https://docs.google.com/document/d/1JBdCl3MAOSdlgq3kzzRmtzTsFWsTIQ9iQg0RHhMht6E/edit)
- [Code Review Weekly Meeting Agenda](https://docs.google.com/document/d/1ICOsbWaRDvOEahLa8Qmqq2OJhq87tCnTHMOPfFK6UTw/edit)
- [Internal Job Board](https://boards.greenhouse.io/gitlab)

### ⏰ Missed Deliverables :new: 

<details><summary>Expand for Missed-Deliverables for: Create:Code Review + Frontend</summary>

```glql
---
display: table
fields: assignee, labels("frontend"), weight, milestone, labels("missed:*", "Deliverable"), title
---
group = "gitlab-org" and label = "group::code review" and label = "frontend" and label = "missed-deliverable" and opened = true and milestone != "Backlog" and milestone != "Next 1-3 releases"
```

</details>

<details><summary>Expand for Missed-Deliverables for: Create:Source Code + Frontend</summary>

```glql
---
display: table
fields: assignee, labels("frontend"), weight, milestone, labels("missed:*", "Deliverable"), title
---
group = "gitlab-org" and label = "group::source code" and label = "frontend" and label = "missed-deliverable" and opened = true and milestone != "Backlog" and milestone != "Next 1-3 releases"
```

</details>

## :dart: OKRs

* **FY26-Q1** Stand by...
* **FY25-Q4** 
   * [Create:Source Code Management](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=title_asc&state=opened&label_name%5B%5D=FY25-Q4&label_name%5B%5D=group::source%20code&assignee_username%5B%5D=andr3&first_page_size=20)
   * [Create:Code Review](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=title_asc&state=opened&label_name%5B%5D=FY25-Q4&label_name%5B%5D=group::code%20review&assignee_username%5B%5D=andr3&first_page_size=20)

## :mega: Weekly Announcements from Director of Engineering - Create (Darva)

<!-- PASTE THE GLOBAL UPDATES HERE -->

/assign @iamphill @thomasrandolph @slashmanov @jerasmus @psjakubowska @ms.mondrian
/due in 1 week
/confidential
